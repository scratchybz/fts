package fts

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/parnurzeal/gorequest"
	"github.com/pkg/errors"
)

//Client - creates a new Client struct
type Client struct {
	authentication       *authentication
	token                string
	debug                bool
	debugHTTPRequest     bool
	debugLineLength      int64
	logger               logger
	logFtsResponse       bool
	error                *Error
	response             *response
	Sender               *gorequest.SuperAgent
	transactionID        string
	baseURL              *url.URL
	requestRetryAttempts int
	requestRetryTimeout  time.Duration
}

const (
	StatusCodeOk            = 200
	POST                    = "POST"
	GET                     = "GET"
	HEAD                    = "HEAD"
	PUT                     = "PUT"
	DELETE                  = "DELETE"
	PATCH                   = "PATCH"
	OPTIONS                 = "OPTIONS"
	TOKEN                   = "token"
	TransactionID           = "transactionId"
	CodeSuccess             = 0
	CodeGeneralError        = 1
	CodeAuthenticationError = 6
)

type logger interface {
	Print(v ...interface{})
}

//New - Create a new Client struct.
func NewClient(baseURL *url.URL) *Client {

	authResponse := &response{}
	authResponse.SetCode(CodeGeneralError)

	Client := &Client{
		authentication: &authentication{
			Response: authResponse,
		},
		token: "",
		error: &Error{},
		response: &response{
			Code: CodeGeneralError,
		},
		Sender:           gorequest.New().SetDebug(true),
		logFtsResponse:   false,
		baseURL:          baseURL,
		debug:            false,
		debugHTTPRequest: false,
	}

	//Client.Sender.SetDebug(true)

	return Client
}

// NewBasicAuthClient
func NewBasicAuthClient(baseURL *url.URL, username string, password string) *Client {
	return NewClient(baseURL).SetBasicAuth(username, password)
}

func (c *Client) SetFTSTimeout(seconds int) {
	c.Sender.Timeout(time.Duration(seconds) * time.Second)
}

// SetBasicAuth sets the basic authentication header
func (c *Client) SetBasicAuth(username string, password string) *Client {
	return c.SetUserName(username).SetPassword(password)
}

// SetUserName sets the Username that will be used for authentication
func (c *Client) SetUserName(username string) *Client {
	c.authentication.SetUsername(username)
	return c
}

// SetLogger - Set the logger to be used
func (c *Client) SetLogger(logger logger) *Client {
	c.logger = logger
	return c
}

// SetPassword sets the Password that will be used for authentication
func (c *Client) SetPassword(password string) *Client {
	c.authentication.SetPassword(password)
	return c
}

//SetLogAPIResponse - Sets the log response for FTS Client - displays FTS response if Set to True
func (c *Client) SetLogAPIResponse(logAPIResponse bool) *Client {
	c.logFtsResponse = logAPIResponse
	return c
}

//IsAuthenticated - Checks if the client is authenticated
func (c *Client) IsAuthenticated() bool {
	if tokenEmpty(c) {
		return false
	}
	return c.authentication.IsAuthenticated()
}

// SetDebug - Set whether to debug request and response
func (c *Client) SetDebug(debug bool) *Client {
	c.debug = debug
	return c
}

// SetDebugLineLength
func (c *Client) SetDebugLineLength(charLength int64) *Client {
	c.debugLineLength = charLength
	return c
}

// SetHttpRequestDebug
func (c *Client) SetHttpRequestDebug(debug bool) *Client {
	c.debugHTTPRequest = debug
	//c.Sender.SetDebug(debug)
	return c
}

// SetRequestRetryAttempts
func (c *Client) SetRequestRetryAttempts(retryAttempts int) *Client {
	c.requestRetryAttempts = retryAttempts
	return c
}

// SetRequestRetryTimeout
func (c *Client) SetRequestRetryTimeout(timeout time.Duration) *Client {
	c.requestRetryTimeout = timeout
	return c
}

// HasToken
func (c *Client) HasToken() bool {
	return len(c.token) > 0
}

// Authenticate - authenticate API request
func (c *Client) Authenticate() (bool, error) {
	username := c.authentication.GetUsername()
	password := c.authentication.GetPassword()
	return c.AuthenticateWithBasicAuth(username, password)
}

// AuthenticateWithBasicAuth - authenticate API request
func (c *Client) AuthenticateWithBasicAuth(username string, password string) (bool, error) {

	if username == "" {
		userErr := "The FTS authentication username is empty"
		c.response.SetMessage("Username is required")
		c.response.SetDescription(userErr)
		c.response.SetCode(-1)
		return false, errors.New(userErr)
	}

	if password == "" {
		passErr := "The FTS authentication password is empty"
		c.response.SetMessage("Username and Password are required")
		c.response.SetDescription(passErr)
		c.response.SetCode(-1)

		return false, errors.New(passErr)
	}

	// clear token before authenticating
	c.token = ""

	apiParams := map[string]interface{}{"userName": username, "userPassword": password}

	body, errs := c.CallAPI("Authenticate", apiParams) //(*http.response, string, error)

	if errs != nil {
		return false, errs
	}

	unmarshalString(body, &c.authentication.Response)

	if c.authentication.GetResponseCode() != CodeSuccess {
		if c.debug {
			c.print(fmt.Sprintf("[error][FTS Client] Failed to authenticate %s %v", body, errs))
		}
		c.authentication.SetAuthenticated(false)
		return false, errors.New(c.authentication.GetResponseDescription())
	}

	c.SetBasicAuth(username, password)

	c.authentication.SetAuthenticated(true)

	c.token = c.authentication.Response.GetResult()

	if c.debug {
		c.print(fmt.Sprintf("[FTS Client] Successfully authenticated. token is %s", c.token))
	}

	return true, nil
}

func (c *Client) mapToApiParamsQuery(apiParams map[string]interface{}) string {
	// check if token present
	_, tokenPresent := apiParams[TOKEN]

	if c.token != "" && !tokenPresent {
		apiParams[TOKEN] = c.token
	}

	// check if transaction id present
	_, transIdPresentInParams := apiParams[TransactionID]

	if c.transactionID != "" && !transIdPresentInParams {
		apiParams[TransactionID] = c.transactionID
	}

	return fmt.Sprintf(`api_params={%s}`, mapToStringApiParams(apiParams))
}

// CallAPI - Make an API request
func (c *Client) CallAPI(apiName string, apiParams map[string]interface{}) (string, error) {

	apiNameQuery := fmt.Sprintf(`api_name=%s`, apiName)
	apiParamsQuery := c.mapToApiParamsQuery(apiParams)

	c.Sender.SetDebug(c.debugHTTPRequest)
	c.method(POST, c.baseURL.String())

	if c.debug {
		c.print(fmt.Sprintf("[FTS Client][%s]: API Request: %s. Awaiting HTTP response ...", apiName, c.mapToApiParamsQuery(maskPin(apiParams))))
	}

	timestarted := time.Now()

	reqResp, body, reqErrs := c.sendRequest(apiNameQuery, apiParamsQuery)

	timeended := time.Now()

	if reqErrs != nil {
		//c.print(fmt.Sprintf("%+v", reqErrs))

		for _, err := range reqErrs {
			if os.IsTimeout(err) {
				c.print(fmt.Sprintf("[ERROR][FTS Client][%s]: %s - No response from FTS", apiName, err.Error()))
			} else {
				c.print(fmt.Sprintf("[ERROR][FTS Client][%s]: %s", apiName, err.Error()))
			}
		}
		c.print(fmt.Sprintf("[DEBUG][FTS Client][%s]: Time taken is: %s ", apiName, timeended.Sub(timestarted)))
		return body, reqErrs[0]
	}

	defer reqResp.Body.Close()

	c.logAPIResponse(apiName, reqResp)

	unmarshalString(body, &c.response)

	if reqResp == nil || reqResp.StatusCode != StatusCodeOk || reqErrs != nil { // failed to send bytes or code is not ok
		return body, reqErrs[0]
	}

	return body, nil

}

func (c *Client) logAPIResponse(apiName string, reqResp *http.Response) {

	if c.debug {

		responseLogFormat := "[FTS Client][%s]: response: status: %s, code: %d, body: %s"

		if c.debugLineLength == 0 {
			c.print(fmt.Sprintf(responseLogFormat, apiName, reqResp.Status, reqResp.StatusCode, reqResp.Body))
		}

		if c.debugLineLength > 0 {
			buf := new(bytes.Buffer)
			buf.ReadFrom(reqResp.Body)
			stringToWriteToLog := buf.String()
			stringToWriteToLogLength := int64(len(stringToWriteToLog))

			if c.debugLineLength < stringToWriteToLogLength {
				c.print(fmt.Sprintf(responseLogFormat, apiName, reqResp.Status, reqResp.StatusCode, stringToWriteToLog[0:c.debugLineLength]))
			} else {
				c.print(fmt.Sprintf(responseLogFormat, apiName, reqResp.Status, reqResp.StatusCode, stringToWriteToLog))
			}
		}
	}
}

// NeedsReauthentication - Checks if client has been logged in before and needs to be authenticated now
func (c *Client) NeedsReauthentication(responseCode int) bool {

	if c.NeedsAuthentication() || CodeAuthenticationError == responseCode {
		return true
	}

	return false
}

// NeedsAuthentication - Checks if client needs to be authenticated
func (c *Client) NeedsAuthentication() bool {

	if c.HasToken() == false || c.authentication.IsAuthenticated() == false {
		return true
	}

	return false
}

func (c *Client) sendRequest(apiNameQuery string, apiParamsQuery string) (*http.Response, string, []error) {

	c.Sender.Send(apiNameQuery).Send(apiParamsQuery)

	c.Sender.Retry(c.requestRetryAttempts, c.requestRetryTimeout, http.StatusBadRequest, http.StatusInternalServerError)

	reqResp, body, reqErrs := c.Sender.End()

	return reqResp, body, reqErrs
}

// tokenEmpty - checks whether the client token is empty
func tokenEmpty(c *Client) bool {
	return len(c.token) < 1
}

//method - set the method  and target url for current request
func (c *Client) method(method string, targetUrl string) *gorequest.SuperAgent {

	switch strings.ToUpper(method) {
	case POST:
		c.Sender.Post(targetUrl)
	case GET:
		c.Sender.Get(targetUrl)
	case PUT:
		c.Sender.Put(targetUrl)
	case PATCH:
		c.Sender.Patch(targetUrl)
	case DELETE:
		c.Sender.Delete(targetUrl)
	}

	return c.Sender
}

func mapToStringApiParams(apiParams map[string]interface{}) string {

	var paramsBuffer bytes.Buffer

	for k, v := range apiParams {
		paramsBuffer.WriteString(fmt.Sprintf(`"%s":"%v",`, k, v)) // form key value from map and append to buffer
	}

	return strings.TrimRight(paramsBuffer.String(), ",")
}

func ummarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, &v)
}

func unmarshalString(data string, v interface{}) error {
	return ummarshal([]byte(data), v)
}

func (c *Client) print(message string) {

	if c.logger != nil {
		c.logger.Print(message)
	} else {
		log.Print(message)
	}
}

func maskPin(apiParams map[string]interface{}) map[string]interface{} {
	// mask pin
	if _, ok := apiParams["Pin"]; ok {
		apiParams["Pin"] = "******"
	}
	if _, ok := apiParams["pin"]; ok {
		apiParams["pin"] = "******"
	}
	return apiParams
}
