package fts

import "errors"

// GetSubscriberList
func (c *Client) GetSubscriberList(apiParams map[string]interface{}) ([]Subscriber, error) {
	subListResponse := GetSubscriberListResponse{
		Result: []Subscriber{},
		Code:   CodeGeneralError,
	}

	body, errors := c.CallAPI("GetSubscriberList", apiParams)

	unmarshalString(body, &subListResponse)

	if errors != nil {
		return nil, errors
	}

	return subListResponse.Result, nil
}

// GetCustomer
func (c *Client) GetCustomer(apiParams map[string]interface{}) (Customer, int, error) {

	custResponse := GetCustomerResponse{
		Result: Customer{},
	}

	body, err := c.CallAPI("GetCustomer", apiParams)

	unmarshalString(body, &custResponse)

	if err != nil {
		return custResponse.Result, CodeGeneralError, err
	}

	if custResponse.Code != CodeSuccess {
		return custResponse.Result, custResponse.Code, errors.New(custResponse.Message)
	}

	return custResponse.Result, CodeSuccess, nil
}

// SuspendSubscriber
func (c *Client) SuspendSubscriber(apiParams map[string]interface{}) (bool, error) {

	body, err := c.CallAPI("SuspendSubscriber", apiParams)

	response := response{
		Code: CodeGeneralError,
	}

	unmarshalString(body, &response)

	if err != nil {
		return false, err
	}

	if response.GetCode() != CodeSuccess {
		return false, errors.New(response.GetMessage())
	}

	return true, nil
}

// SubscriberPay
func (c *Client) SubscriberPay(apiParams map[string]interface{}) (bool, error) {

	body, err := c.CallAPI("SubscriberPay", apiParams)

	response := response{
		Code: CodeGeneralError,
	}

	unmarshalString(body, &response)

	if err != nil {
		return false, err
	}

	if response.GetCode() != CodeSuccess {
		return false, errors.New(response.GetMessage() + " - " + response.GetDescription())
	}

	return true, nil
}

// CreateSubscriberBalanceTransfer
func (c *Client) CreateSubscriberBalanceTransfer(apiParams map[string]interface{}) (SubscriberTransferBalance, int, error) {

	balance := SubscriberTransferBalance{}

	response := SubscriberBalanceTransferResponse{
		Result: balance,
		Code:   CodeGeneralError,
	}
	body, err := c.CallAPI("CreateSubscriberBalanceTransfer", apiParams)

	unmarshalString(body, &response)

	if err != nil {
		return balance, CodeGeneralError, err
	}

	if response.Code != CodeSuccess {
		return balance, response.Code, errors.New(response.Message + " - " + response.Description)
	}

	return response.Result, CodeSuccess, nil
}

// GetCustomerBankDisplayList
func (c *Client) GetCustomerBankDisplayList(apiParams map[string]interface{}) ([]CustomerBank, error) {

	response := GetCustomerBankDisplayListResponse{
		Result: []CustomerBank{},
		Code:   CodeGeneralError,
	}
	body, error := c.CallAPI("GetCustomerBankDisplayList", apiParams)

	unmarshalString(body, &response)

	if error != nil {
		return nil, error
	}

	if response.Code != CodeSuccess {
		return nil, errors.New(response.Description)
	}

	return response.Result, nil

}

// GetSubProductsServices
func (c *Client) GetSubProductsServices(apiParams map[string]interface{}) ([]SubscriberProductService, error) {

	subProdResponse := SubscriberProdServicesResponse{
		Result: []SubscriberProductService{},
		Code:   CodeGeneralError,
	}

	body, error := c.CallAPI("GetSubProductsServices", apiParams)

	unmarshalString(body, &subProdResponse)

	if error != nil {
		return nil, error
	}

	if subProdResponse.Code != CodeSuccess {
		return nil, errors.New(subProdResponse.Message + " - " + subProdResponse.Description)
	}

	return subProdResponse.Result, nil

}

// GetBankDisplayList
func (c *Client) GetBankDisplayList(apiParams map[string]interface{}) ([]DisplayBanks, error) {

	response := BankDisplayListResponse{
		Result: []DisplayBanks{},
	}

	body, error := c.CallAPI("GetBankDisplayList", apiParams)

	unmarshalString(body, &response)

	if error != nil {
		return nil, error
	}

	if response.Code != CodeSuccess {
		return nil, errors.New(response.Description)
	}

	return response.Result, nil

}

//TransactionStart - starts a new transaction
func (c *Client) TransactionStart(apiParams map[string]interface{}) (bool, error) {

	body, err := c.CallAPI("StartTransaction", apiParams)

	response := response{}

	unmarshalString(body, &response)

	if err != nil {
		return false, err
	}

	if response.GetCode() != CodeSuccess {
		return false, errors.New(response.GetDescription())
	}

	c.transactionID = response.GetResult() // c.GetRequestResponse()
	return true, nil
}

//TransactionCommit - commits transaction
func (c *Client) TransactionCommit() (bool, error) {
	transactionParams := map[string]interface{}{"transactionId": c.transactionID}

	body, err := c.CallAPI("Commit", transactionParams)

	response := response{}

	if err != nil {
		return false, err
	}

	unmarshalString(body, &response)

	if response.GetCode() != CodeSuccess {
		return false, errors.New(response.GetDescription())
	}

	c.transactionID = "" //clear transaction after successful commit
	return true, nil

}

//TransactionRollback - roll back a transaction
func (c *Client) TransactionRollback() (bool, error) {
	transactionParams := map[string]interface{}{"transactionId": c.transactionID}
	response := response{}

	body, err := c.CallAPI("Rollback", transactionParams)

	if err != nil {
		return false, err
	}

	unmarshalString(body, &response)

	if response.GetCode() != CodeSuccess {
		return false, errors.New(response.GetDescription())
	}

	c.transactionID = "" // clear transaction ID after rollback

	return true, nil

}
