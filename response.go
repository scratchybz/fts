package fts

// response - Structure used to store the response data for each request for the Client
type response struct {
	Result      string `json:"return_result"`
	Message     string `json:"return_message"`
	Description string `json:"return_description"`
	Code        int    `json:"return_code"`
}

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (r *response) SetResult(result string) *response {
	r.Result = result
	return r
}

func (r *response) GetResult() string {
	return r.Result
}

func (r *response) SetMessage(message string) *response {
	r.Message = message
	return r
}

func (r *response) GetMessage() string {
	return r.Message
}

func (r *response) SetDescription(description string) *response {
	r.Description = description
	return r
}

func (r *response) GetDescription() string {
	return r.Description
}

func (r *response) SetCode(code int) *response {
	r.Code = code
	return r
}

func (r *response) GetCode() int {
	return r.Code
}
