package fts

// authentication - Structure used to store authentication data for Client
type authentication struct {
	Authenticated bool      `json:"status",omitempty"`
	Response      *response `json:"response",omitempty"`
	Username      string    `json:"userName",omitempty"`
	Password      string    `json:"userPassword",omitempty"`
	//error    *error    `json:"error",omitempty"`
}

func (auth *authentication) SetAuthenticated(isAuthenticated bool) *authentication {
	auth.Authenticated = isAuthenticated
	return auth
}

func (auth *authentication) IsAuthenticated() bool {
	return auth.Authenticated
}

func (auth *authentication) SetResponse(response *response) *authentication {
	auth.Response = response
	return auth
}

func (auth *authentication) GetResponse() *response {
	return auth.Response
}

func (auth *authentication) SetResponseCode(code int) *authentication {
	auth.GetResponse().SetCode(code)
	return auth
}

func (auth *authentication) GetResponseCode() int {
	return auth.GetResponse().GetCode()
}

func (auth *authentication) SetResponseResult(result string) *authentication {
	auth.GetResponse().SetResult(result)
	return auth
}

func (auth *authentication) GetResponseResult() string {
	return auth.GetResponse().GetResult()
}

func (auth *authentication) SetResponseMessage(message string) *authentication {
	auth.GetResponse().SetMessage(message)
	return auth
}

func (auth *authentication) GetResponseMessage() string {
	return auth.GetResponse().GetMessage()
}

func (auth *authentication) SetResponseDescription(description string) *authentication {
	auth.GetResponse().SetDescription(description)
	return auth
}

func (auth *authentication) GetResponseDescription() string {
	return auth.GetResponse().GetDescription()
}

func (auth *authentication) SetUsername(username string) *authentication {
	auth.Username = username
	return auth
}

func (auth *authentication) GetUsername() string {
	return auth.Username
}

func (auth *authentication) SetPassword(password string) *authentication {
	auth.Password = password
	return auth
}

func (auth *authentication) GetPassword() string {
	return auth.Password
}
