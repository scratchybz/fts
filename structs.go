package fts

//Subscriber - Structure of an fts subscriber
type Subscriber struct {
	ValidationInd          int    `json:"validation_ind"`
	ValidationTime         string `json:"validation_time"`
	MinRuntime             int    `json:"min_runtime"`
	ClosureDate            string `json:"closure_date"`
	PinCode                string `json:"pin_code"`
	PromotionID            string `json:"promotion_id"`
	AgreementVersion       string `json:"agreement_version"`
	OriginalSubscObjNum    int    `json:"original_subsc_obj_num"`
	SuspendExpDate         string `json:"suspend_exp_date"`
	MainResource           string `json:"main_resource"`
	ActivationInd          int    `json:"activation_ind"`
	SubscriberID           string `json:"subscriber_id"`
	Mabal                  string `json:"mabal"`
	CustMainSubInd         bool   `json:"cust_main_sub_ind"`
	SuspendEffDate         string `json:"suspend_eff_date"`
	CalledNumberDisplay    int    `json:"called_number_display"`
	SubscriberExtID        string `json:"subscriber_ext_id"`
	DealerID               string `json:"dealer_id"`
	RegistrationSource     string `json:"registration_source"`
	LastHistoryDate        string `json:"last_history_date"`
	SimType                string `json:"sim_type"`
	OID                    string `json:"OID"`
	CreationDate           string `json:"creation_date"`
	CallDetailsType        int    `json:"call_details_type"`
	ActivationTime         string `json:"activation_time"`
	SecondResource         string `json:"second_resource"`
	ProvisionID            string `json:"provision_id"`
	RegistrationDate       string `json:"registration_date"`
	HasCustProduct         bool   `json:"has_cust_product"`
	CostCenter             string `json:"cost_center"`
	CreationUserID         string `json:"creation_user_id"`
	CustomerObjNum         int64  `json:"customer_obj_num"`
	StatusChangeReason     string `json:"status_change_reason"`
	CallStorageInd         int    `json:"call_storage_ind"`
	SubscriberResourceType int    `json:"subscriber_resource_type"`
	Status                 int    `json:"status"`
	DefaultTariffID        string `json:"default_tariff_id"`
}

type GetSubscriberListResponse struct {
	Result      []Subscriber `json:"return_result"`
	Message     string       `json:"return_message"`
	Description string       `json:"return_description"`
	Code        int          `json:"return_code"`
}

type Bank struct {
	LastCycleOpenInd bool    `json:"last_cycle_open_ind"`
	UnitSign         string  `json:"unit_sign"`
	BankType         int     `json:"bank_type"`
	WriteTxInd       bool    `json:"write_tx_ind"`
	BankPrecision    string  `json:"bank_precision"`
	Description      string  `json:"description"`
	BankObjNum       int64   `json:"bank_obj_num"`
	OID              string  `json:"OID"`
	UnitDescription  string  `json:"unit_description"`
	UsageOrientation int     `json:"usage_orientation"`
	MainBankInd      int     `json:"main_bank_ind"`
	NextEventBalance int     `json:"next_event_balance"`
	Bank             string  `json:"bank"`
	Balance          float64 `json:"balance"`
	CustomerObjNum   int64   `json:"customer_obj_num"`
	CreditLimit      int     `json:"credit_limit"`
	LastCycleDate    string  `json:"last_cycle_date"`
	PartID           string  `json:"part_id"`
	NextCycleDate    string  `json:"next_cycle_date"`
	BalanceTierInd   bool    `json:"balance_tier_ind"`
	VisibilityType   int     `json:"visibility_type"`
	NextEvent        int     `json:"next_event"`
	// Params added after fix
	CurrencyInd        bool   `json:"currency_ind"`
	LastArchiveBalance int    `json:"last_archive_balance"`
	Precision          string `json:"precision"`
	AttributeObjNum    int64  `json:"attribute_obj_num"`
	BankName           string `json:"bank_name"`
	AttributeName      string `json:"attribute_name"`
	ExternalTxBalance  int    `json:"external_tx_balance"`
	LastArchiveDate    string `json:"last_archive_date"`
	InvoicedInd        bool   `json:"invoiced_ind"`
	SourceType         string `json:"source_type"`
}

type DisplayBanks struct {
	TxBanks []struct {
		LastCycleOpenInd   bool    `json:"last_cycle_open_ind"`
		CurrencyInd        bool    `json:"currency_ind"`
		UnitSign           string  `json:"unit_sign"`
		LastArchiveBalance float64 `json:"last_archive_balance"`
		Precision          string  `json:"precision"`
		WriteTxInd         bool    `json:"write_tx_ind"`
		AttributeObjNum    int64   `json:"attribute_obj_num"`
		BankObjNum         int64   `json:"bank_obj_num"`
		Balance            float64 `json:"balance"`
		BankName           string  `json:"bank_name"`
		CreditLimit        int     `json:"credit_limit"`
		AttributeName      string  `json:"attribute_name"`
		PartID             string  `json:"part_id"`
		BalanceTierInd     bool    `json:"balance_tier_ind"`
		NextEvent          int     `json:"next_event"`
		ExternalTxBalance  int     `json:"external_tx_balance"`
		LastArchiveDate    string  `json:"last_archive_date"`
		BankType           int     `json:"bank_type"`
		InvoicedInd        bool    `json:"invoiced_ind"`
		SourceType         string  `json:"source_type"`
		OID                string  `json:"OID"`
		UsageOrientation   int     `json:"usage_orientation"`
		MainBankInd        bool    `json:"main_bank_ind"`
		NextEventBalance   int     `json:"next_event_balance"`
		CustomerObjNum     int64   `json:"customer_obj_num"`
		LastCycleDate      string  `json:"last_cycle_date"`
		NextCycleDate      string  `json:"next_cycle_date"`
	} `json:"tx_banks,omitempty"`
	CyclicBanks []interface{} `json:"cyclic_banks,omitempty"`
}

//BankDisplayListResponse - Structure of a bank response from fts api
type BankDisplayListResponse struct {
	Description string         `json:"return_description"`
	Result      []DisplayBanks `json:"return_result"`
	Message     string         `json:"return_message"`
	Code        int            `json:"return_code"`
}

type SubscriberProductService struct {
	ProductSiblings     string `json:"product_siblings,omitempty"`
	CustProductStatus   string `json:"cust_product_status"`
	CustServiceOid      string `json:"cust_service_oid"`
	ProductOid          string `json:"product_oid"`
	CustProductName     string `json:"cust_product_name"`
	ProductName         string `json:"product_name"`
	CustServiceStatus   string `json:"cust_service_status"`
	PccRuleInd          bool   `json:"pcc_rule_ind"`
	GlobalServiceInd    bool   `json:"global_service_ind"`
	CustServicePurchase string `json:"cust_service_purchase"`
	CustProductOid      string `json:"cust_product_oid"`
	ServiceSiblings     string `json:"service_siblings,omitempty"`
	CustServiceName     string `json:"cust_service_name"`
}

//SubscriberProdServicesResponse - Structure of the subscriber services products
type SubscriberProdServicesResponse struct {
	Description string                     `json:"return_description"`
	Result      []SubscriberProductService `json:"return_result"`
	Message     string                     `json:"return_message"`
	Code        int                        `json:"return_code"`
}

type Customer struct {
	Country            string `json:"country"`
	BillingLastName    string `json:"billing_last_name"`
	BillingMainPhone   string `json:"billing_main_phone"`
	City               string `json:"city"`
	County             string `json:"county"`
	BillingZipCode     string `json:"billing_zip_code"`
	BillingAddress     string `json:"billing_address"`
	SuspendExpDate     string `json:"suspend_exp_date"`
	ZipCode            string `json:"zip_code"`
	StatusEffDate      string `json:"status_eff_date"`
	BillingState       string `json:"billing_state"`
	SuspendEffDate     string `json:"suspend_eff_date"`
	Tin                string `json:"tin"`
	MarketObjNum       int64  `json:"market_obj_num"`
	UseHomeAddressInd  bool   `json:"use_home_address_ind"`
	MainPhone          string `json:"main_phone"`
	State              string `json:"state"`
	BillingFirstName   string `json:"billing_first_name"`
	FirstName          string `json:"first_name"`
	JobTitle           string `json:"job_title"`
	PaymentMethod      int    `json:"payment_method"`
	BillingAddressTwo  string `json:"billing_address_two"`
	Address            string `json:"address"`
	ExternalCustID     string `json:"external_cust_id"`
	BillingCountry     string `json:"billing_country"`
	BillingCounty      string `json:"billing_county"`
	OID                string `json:"OID"`
	AddressTwo         string `json:"address_two"`
	MiddleName         string `json:"middle_name"`
	BillingCity        string `json:"billing_city"`
	BillingJobTitle    string `json:"billing_job_title"`
	RegistrationDate   string `json:"registration_date"`
	BillingTitle       string `json:"billing_title"`
	StatusChangeReason string `json:"status_change_reason"`
	SecondName         string `json:"second_name"`
	NextCycleDate      string `json:"next_cycle_date"`
	Salutation         int    `json:"salutation"`
	LastCycleDate      string `json:"last_cycle_date"`
	BillingMiddleName  string `json:"billing_middle_name"`
}

//SubscriberInfoResponse - Structure of the subscriber info response from fts api
type SubscriberInfoResponse struct {
	ReturnDescription string `json:"return_description"`
	ReturnResult      []struct {
		Subscriber struct {
			MainResource       string `json:"main_resource"`
			RegistrationDate   string `json:"registration_date"`
			SubscriberID       string `json:"subscriber_id"`
			ClosureDate        string `json:"closure_date"`
			SuspendEffDate     string `json:"suspend_eff_date"`
			StatusChangeReason string `json:"status_change_reason"`
			OID                string `json:"OID"`
			SuspendExpDate     string `json:"suspend_exp_date"`
			Status             int    `json:"status"`
		} `json:"subscriber,omitempty"`
		Customer *Customer `json:"customer,omitempty"`
		Banks    []Bank    `json:"banks,omitempty"`
	} `json:"return_result"`
	ReturnMessage string `json:"return_message"`
	ReturnCode    int    `json:"return_code"`
}

type CustomerBank struct {
	TxBanks []struct {
		LastCycleOpenInd bool    `json:"last_cycle_open_ind"`
		UnitSign         string  `json:"unit_sign"`
		BankType         int     `json:"bank_type"`
		WriteTxInd       bool    `json:"write_tx_ind"`
		BankPrecision    string  `json:"bank_precision"`
		Description      string  `json:"description"`
		BankObjNum       int64   `json:"bank_obj_num"`
		OID              string  `json:"OID"`
		UnitDescription  string  `json:"unit_description"`
		UsageOrientation int     `json:"usage_orientation"`
		MainBankInd      int     `json:"main_bank_ind"`
		NextEventBalance int     `json:"next_event_balance"`
		Bank             string  `json:"bank"`
		Balance          float64 `json:"balance"`
		CustomerObjNum   int64   `json:"customer_obj_num"`
		CreditLimit      int     `json:"credit_limit"`
		LastCycleDate    string  `json:"last_cycle_date"`
		PartID           string  `json:"part_id"`
		NextCycleDate    string  `json:"next_cycle_date"`
		BalanceTierInd   bool    `json:"balance_tier_ind"`
		VisibilityType   int     `json:"visibility_type"`
		NextEvent        int     `json:"next_event"`
		// Params added after fix
		CurrencyInd        bool   `json:"currency_ind"`
		LastArchiveBalance int    `json:"last_archive_balance"`
		Precision          string `json:"precision"`
		AttributeObjNum    int64  `json:"attribute_obj_num"`
		BankName           string `json:"bank_name"`
		AttributeName      string `json:"attribute_name"`
		ExternalTxBalance  int    `json:"external_tx_balance"`
		LastArchiveDate    string `json:"last_archive_date"`
		InvoicedInd        bool   `json:"invoiced_ind"`
		SourceType         string `json:"source_type"`
	} `json:"tx_banks,omitempty"`
	CyclicBanks []interface{} `json:"cyclic_banks,omitempty"`
}

//CustBankResponse - Structure of a bank response from fts api
type GetCustomerBankDisplayListResponse struct {
	Description string         `json:"return_description"`
	Result      []CustomerBank `json:"return_result"`
	Message     string         `json:"return_message"`
	Code        int            `json:"return_code"`
}

type GetCustomerResponse struct {
	Description string   `json:"return_description"`
	Result      Customer `json:"return_result"`
	Message     string   `json:"return_message"`
	Code        int      `json:"return_code"`
}

type SubscriberTransferBalance struct {
	DestEndingBalance   float32 `json:"dest_ending_balance"`
	DestStartingBalance float32 `json:"dest_starting_balance"`
	SrcEndingBalance    float32 `json:"src_ending_balance"`
	SrcStartingBalance  float32 `json:"src_starting_balance"`
}

type SubscriberBalanceTransferResponse struct {
	Code        int                       `json:"return_code"`
	Description string                    `json:"return_description"`
	Message     string                    `json:"return_message"`
	Result      SubscriberTransferBalance `json:"return_result"`
}

type AuthenticationResponse struct {
	Result      string `json:"return_result"`
	Message     string `json:"return_message"`
	Description string `json:"return_description"`
	Code        int    `json:"return_code"`
	//Body        string `json:"body",omitempty"`
	//Timestamp   string `json:"return_timestamp",omitempty"`
}
