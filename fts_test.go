package fts

import (
	"testing"
	"net/url"
	//	"log"
	"log"
)

var testURL *url.URL

const (
	validFtsUsername = "vcayetano"
	validFtsPassword = "devpass123"
)

func TestNewClient(t *testing.T) {

	client := NewClient(ftsURL())

	if client.authentication.GetPassword() != "" {
		t.Error("Expected client Password to be empty")
	}

	if client.authentication.GetUsername() != "" {
		t.Error("Expected client Password to be empty")
	}
}

func TestNewBasicAuthClient(t *testing.T) {

	client := NewBasicAuthClient(ftsURL(), "test", "Password")

	if client.authentication.GetPassword() == "" {
		t.Error("Expected client Password to be empty")
	}

	if client.authentication.GetUsername() == "" {
		t.Error("Expected client Password to be empty")
	}
}

func TestClient_CallApi(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	_, body, err := client.CallAPI("Authenticate", map[string]interface{}{"userName": client.authentication.GetUsername(), "userPassword": client.authentication.GetPassword()})

	if err != nil {
		t.Errorf("Expected no errors, but received %v", err)
	}

	if body == "" {
		t.Error("Expected body have content but was empty")
	}

}

func TestClient_CallApiWithNoToken(t *testing.T) {
	c := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	c.Authenticate()
	c.token = ""

	_, errcode, _ := c.GetCustomer(map[string]interface{}{"identifier": "5016784365"})

	noTokenCode := 4
	if errcode != noTokenCode {
		t.Errorf("Expected 'no token' error code %d", noTokenCode)
	}

}

func TestClient_CallApiWithInvalidToken(t *testing.T) {
	c := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	c.Authenticate()
	//c.SetDebug(true)
	c.token = "welcodafkfdamf98da9f8d"

	_, errcode, _ := c.GetCustomer(map[string]interface{}{"identifier": "5016784365"})

	autFailureCode := 6
	if errcode != autFailureCode {
		t.Errorf("Expected invalid token error code %d", autFailureCode)
	}

}

func TestClient_AuthenticateWithBasicAuth(t *testing.T) {

	client := NewBasicAuthClient(ftsURL(), "Invalid user", "Invalid Password")
	//client.SetDebug(true)
	authenticated, erro := client.AuthenticateWithBasicAuth(validFtsUsername, validFtsPassword)

	if client.authentication.GetUsername() != validFtsUsername || client.authentication.GetPassword() != validFtsPassword {
		t.Errorf("Expected client Username '%s' to be '%s', and Password '%s' to be '%s'", client.authentication.GetUsername(), validFtsUsername, client.authentication.GetPassword(), validFtsPassword)
	}

	if authenticated == false || erro != nil {
		t.Errorf("Expected authentication to pass - errror %v", erro)
	}

	if client.authentication.IsAuthenticated() == false {
		t.Error("Expected response code to be true because authentication should pass")
	}

	if client.authentication.GetResponseCode() == 6 {
		t.Error("Expected token to not return 6")
	}

	if len(client.token) < 1 {
		t.Errorf("Expected client token '%s' to have a value after authentication", client.token)
	}
}

func TestClient_IsAuthenticated(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	//client.SetDebug(true)
	//client.SetHttpRequestDebug(true)
	authenticated, erro := client.Authenticate()

	if authenticated == false || erro != nil {
		t.Errorf("Expected authentication to pass - errror %v", erro)
	}

	if client.authentication.IsAuthenticated() == false {
		t.Error("Expected response code to be true because authentication should pass")
	}

	if client.authentication.GetResponseCode() == 6 {
		t.Error("Expected token to not return 6")
	}

	if len(client.token) < 1 {
		t.Errorf("Expected client token '%s' to have a value after authentication", client.token)
	}

	//client.CallAPI("Authenticate", map[string]string{"userName": client.authentication.GetUsername(), "userPassword": client.authentication.GetPassword()})
}

func TestClient_IsAuthenticatedShouldFail(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), "avcayestanos", validFtsPassword)
	//client.debug = false
	//client.SetDebug(true)
	client.Authenticate()

	if client.authentication.IsAuthenticated() == true {
		t.Error("Expected response code to be false because invalid Username")
	}

	if client.authentication.GetResponseCode() != 6 {
		t.Error("Expected token to not return 6")
	}

	if len(client.token) > 0 {
		t.Errorf("Expected client token '%s' to have a value after authentication", client.token)
	}

	//client.CallAPI("Authenticate", map[string]string{"userName": client.authentication.GetUsername(), "userPassword": client.authentication.GetPassword()})
}

func TestClient_NeedsAuthentication(t *testing.T) {
	client := NewClient(ftsURL())

	if client.NeedsAuthentication() == false {
		t.Error("Expected client to need authentication because we have not logged in yet")
	}
}

func TestClient_NeedsAuthenticationWithInvalidUsernameAndPassword(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), "avcayestanos", validFtsPassword)
	client.Authenticate()

	if client.NeedsAuthentication() == false {
		t.Error("Expected client to need authentication because we have not logged in yet")
	}
}

func TestClient_NeedsReauthentication(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), "avcayestanos", validFtsPassword)

	if client.NeedsReauthentication(0) == false {
		t.Error("Expected needs reauthentication because we have not logged in as yet")
	}
}

func TestClient_NeedsReauthenticationInvalidUsernameAndPassword(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), "avcayestanos", validFtsPassword)
	client.Authenticate()
	//client.debug = false
	//client.SetDebug(true)
	client.authentication.SetAuthenticated(true)
	client.token = "lskmfslgmsmgflsfmglmfslglsmf"

	if client.authentication.IsAuthenticated() == false {
		t.Error("Expected is Authenticated to be true because we forced it")
	}


	if !client.NeedsReauthentication(6) {
		t.Error("Expected Need authentication to return true because we forced auth error code")
	}
}

func TestClient_GetCustomer(t *testing.T) {

	c := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	c.Authenticate()

	customer, _, err := c.GetCustomer(map[string]interface{}{"identifier": "5016784365"})

	if err != nil {
		t.Errorf("Expected no errors, but received %v", err)
	}

	if customer.OID == "" {
		t.Error("Expected customer OID to have a value")
	}

}

func TestClient_GetSubscriberList(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()

	subscribers, _ := client.GetSubscriberList(map[string]interface{}{
		"main_resource": "5016705878",
	})

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if len(subscribers) < 1 {
		t.Errorf("Expected at least 1 subscriber")
	}

	//log.Printf("Subscribers are %+v", subscribers)

}

func TestClient_GetSubscriberListOneSubscriber(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true

	subscribers, _ := client.GetSubscriberList(map[string]interface{}{
		"main_resource": "5016705878",
		"status":        "1",
	})

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if len(subscribers) > 1 {
		t.Errorf("Expected only 1 subscriber")
	}

	//log.Printf("Subscribers info is %+v", subscribers[0])

}

func TestClient_SuspendSubscriber(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true

	suspended, err := client.SuspendSubscriber(map[string]interface{}{
		"identifier": "5016699555",
		"reason":     "testing",
	})

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if err != nil {
		t.Errorf("Expected no error, error returned: %v", err)
	}

	if suspended == false {
		t.Error("Expected account to be suspended")
	}

	//log.Printf("Subscribers info is %+v", subscribers[0])

}

func TestClient_CreateSubscriberBalanceTransfer(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true

	balanceTranferParams := map[string]interface{}{"sourceIdentifier": "5016784365", "targetIdentifier": "5016531313", "amount": "10", "reason": "testing", "targetBank": "Prepaid_Credit", "pin": "705482", "attribute": "BZD"}

	_, code, err := client.CreateSubscriberBalanceTransfer(balanceTranferParams)

	if err != nil {
		t.Errorf("Expected no error than the ones we want, error returned: %v", err)
	}

	acceptedCodes := map[int]string{
		0:  "Success",
		12: "Insufficient",
		13: "invalid pin",
	}

	if _, ok := acceptedCodes[code]; !ok {
		t.Errorf("Expected a valid code invalid: %d", code)
	}

}

func TestClient_GetCustomerBankDisplayList(t *testing.T) {
	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true

	bankParams := map[string]interface{}{"csrRequest": "true", "customer_obj_num": "1260340419421"}

	customerBanks, err := client.GetCustomerBankDisplayList(bankParams)

	if err != nil {
		t.Errorf("Expected no error, error returned: %v", err)
		return
	}

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if len(customerBanks) < 1 {
		t.Errorf("Expected customer to have %d bank", 1)
		return
	}

	log.Printf("%+v", customerBanks)
	if customerBanks[0].TxBanks[0].BankName != "MAIN_BZD" {
		t.Errorf("Expected customer bank name to be %s ", "MAIN_BZD")
	}

}

func TestClient_GetSubProductsServices(t *testing.T) {

	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true
	subscriberProdServParams := map[string]interface{}{"subscriberOid": "0:511:1260342094853"}
	productServices, _ := client.GetSubProductsServices(subscriberProdServParams)

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if len(productServices) < 1 {
		t.Error("Expected 1 or more product services")
		return
	}

	custServiceName := productServices[0].CustServiceName
	expectedCustServiceName := "Main Service Gimmedalla(1260344765464)"
	if custServiceName != expectedCustServiceName {
		t.Errorf("Expected service to be '%s' but '%s' was returned", expectedCustServiceName, custServiceName)
	}
}

func TestClient_GetBankDisplayList(t *testing.T) {
	//GetBankDisplayList

	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()
	//client.debug = true
	bankDisplayParams := map[string]interface{}{"identifier": "5016784365", "csrRequest": "true"}
	banks, _ := client.GetBankDisplayList(bankDisplayParams)

	if client.response.Code != 0 {
		t.Error("Expected response code 0")
		return
	}

	if len(banks) < 1 {
		t.Error("Expected 1 or more banks")
		return
	}

	txBank := banks[0].TxBanks[0]
	expectedValue := "BZD"
	if txBank.AttributeName != expectedValue {
		t.Errorf("Expected service to be '%s' but '%s' was returned", expectedValue, txBank.AttributeName)
	}
}

func TestClient_TransactionStart(t *testing.T) {

	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.Authenticate()

	client.TransactionStart(map[string]interface{}{"category": "API_Admin", "name": "GDollarTransfer", "readOnly": "false"})

	if client.transactionID == ""{
		t.Error("Expected transaction ID to have a value after transaction starts")
	}

}


func TestClient_TransactionCommit(t *testing.T) {

	client := NewBasicAuthClient(ftsURL(), validFtsUsername, validFtsPassword)
	client.SetDebug(true)
	client.Authenticate()

	client.TransactionStart(map[string]interface{}{"category": "API_Admin", "name": "GDollarTransfer", "readOnly": "false"})
	client.TransactionCommit()

	if client.response.Code != 0 {
		t.Error("Expected a return code of 0 after commit")
	}

	if client.transactionID != ""{
		t.Error("Expected transaction ID to be empty after a commit is completed")
	}

}




func ftsURL() *url.URL {
	testURL, _ = url.Parse("http://10.23.5.16:51276/ExternalAPI/Invoke")
	return testURL
}
